rsvg-bench - benchmarking utility for librsvg
=============================================

This is [rsvg-bench][rsvg-bench], a small utility for benchmarking
[librsvg][librsvg], which is [GNOME][gnome]'s library for rendering
Scalable Vector Graphics ([SVG][svg]).

Goals
-----

To benchmark librsvg, we would like to do several things:

* Be able to process many SVG images with a single command.  For
  example, this lets us answer a question like, "how long does version
  N of librsvg take to render a directory full of SVG icons?" — which
  is important for the performance of an application chooser.

* Be able to *repeatedly* process SVG files, for example, "render this
  SVG 1000 times in a row".  This is useful to get accurate timings,
  as a single render may only take a few microseconds and may be hard
  to measure.  It also helps with running profilers, as they will be
  able to get more useful samples if the SVG rendering process runs
  repeatedly for a long time.

* Exercise librsvg's major code paths for parsing and rendering
  separately.  For example, librsvg uses different parts of the XML
  parser depending on whether it is being pushed data, vs. being asked
  to pull data from a stream.  Also, we may only want to benchmark the
  parser but not the renderer; or we may want to parse SVGs only once
  but render them many times after that.

Compiling
---------

Rsvg-bench is a Rust program that can be compiled by typing `cargo build`
while you are in its source directory.  This should be all you
need; Cargo will download and build the dependencies automatically.

Usage
-----

You can run rsvg-bench without installing it.  You can do this by
using

```
$ cargo run -- <options for rsvg-bench>
```

or by using

```
$ ./target/debug/rsvg-bench <options for rsvg-bench>
```

These are equivalent.  For the following discussion, we will show
command lines with just `rsvg-bench` on them, and we will assume that
you are running it in whatever way you prefer.

Running `rsvg-bench` without arguments will print its help on the
command line:

```
USAGE:
    rsvg-bench [FLAGS] [OPTIONS] [inputs]...

FLAGS:
        --pixbuf    Render to a GdkPixbuf instead of a Cairo image surface

OPTIONS:
    -p, --num-parse <num_parse>      Number of times to parse each file [default: 100]
    -r, --num-render <num_render>    Number of times to render each file [default: 100]
    -s, --sleep <sleep_secs>         Number of seconds to sleep before starting to process SVGs [default: 0]

ARGS:
    <inputs>...    Input files or directories
```

### Getting timings

Rsvg-bench does not extract timings by itself.  You can use other
tools to do it.  `/usr/bin/time` is a simple and accurate way (note
that this is different from the `time` command in most shells).

### Benchmarking files

```
$ /usr/bin/time rsvg-bench myfile.svg
```

This will parse and render `myfile.svg` 100 times.  The output will be
similar to this:

```
$ /usr/bin/time ./target/debug/rsvg-bench foo.svg
Will parse each file 100 times
Will render each file 100 times
Rendering to Cairo image surface
Sleeping for 0 seconds before processing SVGs...
Processing files!
Processing "foo.svg"
0.31user 0.00system 0:00.31elapsed 100%CPU (0avgtext+0avgdata 24272maxresident)k
0inputs+0outputs (0major+1339minor)pagefaults 0swaps
```

The last two lines come from `/usr/bin/time`  For this example, the
file was processed 100 times.  The benchmark took 0.31 seconds of user
time (parsing and rendering), and under 1/100 sec of system time (time
spent in the kernel, for example, reading files).

You can specify any number of files in the command line:

```
$ rsvg-bench foo.svg bar.svg ...
```

### Benchmarking all the SVG files in a directory

If you pass a directory name instead of a file name, rsvg-bench will
look for all files with extensions `svg` or `SVG` in that directory
*and all its subdirectories recursively*, and process them.  For
example,

```
$ rsvg-bench /usr/share/icons
```

### Choosing how many times to parse or render

Rsvg-bench supports two options to control how many times each file is
processed:

* `-p N` - causes each file to be parsed `N` times.  The minimum is 1.

* `-r N` - cause each file to be rendered `N` times.  The minimum is 0.

Both options default to 100.

**Benchmarking the parser only:** use `-p 1000 -n 0`, for example.
This will parse each file 1000 times, but not render it.

**Benchmarking the renderer:** use `-p 1 -r 1000`, for example.  This
will parse each file just once (this step can't be skipped!), and
render it 1000 times.

### Sleeping before starting

You can ask rsvg-bench to sleep for a few seconds before actually
starting to process files.  This is useful if you want the
`rsvg-bench` process to be running so that you can attach a profiler
to it.  For example, [sysprof][sysprof] lets you choose an
already-running process to monitor.

* `-s N` - rsvg-bench will sleep for N seconds before starting to
  parse and render files.  The default is 0 (don't sleep; process
  files immediately after startup).

### Exercising particular code paths in librsvg

**Rendering to a GdkPixbuf.** Librsvg's renderer writes to a
[Cairo][cairo] surface by default, but it can be asked to output a
[GdkPixbuf][gdk-pixbuf] instead.  This involves an extra conversion step
after having written the Cairo surface.

* `--pixbuf` - Convert the final image to a GdkPixbuf, so that part of
  the procedure can be profiled as well.

Contributing to rsvg-bench
--------------------------

There is a code of conduct for contributors to rsvg-bench; please see the
file [`code-of-conduct.md`][coc].

Please report bugs in [gitlab.gnome.org's issue tracker][issues].  You
can also fork the repository and create merge requests there.

Maintainers
-----------

The maintainer of rsvg-bench is [Federico Mena Quintero][federico].  Feel
free to contact me for any questions you may have about rsvg-bench, both
its usage and its development.  You can contact me in the following
ways:

* [Mail me][mail] at federico@gnome.org.

* IRC: I am `federico` on `irc.gnome.org` in the `#rust` or
  `#gnome-hackers` channels.  I'm there most weekdays (Mon-Fri)
  starting at about UTC 14:00 (that's 08:00 my time; I am in the UTC-6
  timezone).  If this is not a convenient time for you, feel free to
  [mail me][mail] and we can arrange a time.


[rsvg-bench]: https://gitlab.gnome.org/federico/rsvg-bench
[gnome]: https://www.gnome.org
[librsvg]: https://gitlab.gnome.org/GNOME/librsvg
[svg]: https://en.wikipedia.org/wiki/Scalable_Vector_Graphics
[coc]: code-of-conduct.md
[federico]: https://people.gnome.org/~federico/
[mail]: mailto:federico@gnome.org
[issues]: https://gitlab.gnome.org/federico/rsvg-bench/issues
[sysprof]: https://blogs.gnome.org/chergert/2016/04/19/how-to-sysprof/
[cairo]: https://www.cairographics.org/
[gdk-pixbuf]: https://developer.gnome.org/gdk-pixbuf/stable/
